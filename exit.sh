#!/bin/bash

# dmenu is started with the two possible choices if the user really wants to shut the machine down

[ $(echo -e "No\nYes" | dmenu -i -p "Do you want to shutdown?") \
    == "Yes" ] && sudo poweroff
