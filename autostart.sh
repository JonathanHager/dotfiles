# Set the right screen layout
xrandr --output DVI-I-1 --auto --right-of HDMI-1

# Set the wallpaper
feh --bg-scale /home/jonathan/Bilder/wallpaper.png

# Start some nice programs
compton &
sh /home/jonathan/dotfiles/dwmbar.sh &
